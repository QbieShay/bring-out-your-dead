extends Node2D

func _ready():
	for c in $Windows.get_children():
		c.connect("drop_the_dead", self, "drop_the_dead")

func drop_the_dead(body):
	get_parent().add_child(body)
