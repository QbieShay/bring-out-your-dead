# Bring out your dead

A Trijam #41 entry heavily inspired by Monty Python and the epidemic of
nerdflu during GodotCon Poznań 2019.

## License

The game project and original assets are licensed under the MIT license.

## Thirdparty assets

assets/churchbell.wav:
  CC-BY-SA 3.0, Ulrich Metzner & qubodup
  https://opengameart.org/content/church-bell
assets/gaslamp_funworks.ogg:
  CC-BY 3.0, Kevin MacLeod (incompetech.com)
  https://incompetech.com/music/royalty-free/index.html?isrc=USUAN1100826
assets/FiraSans-Regular.ttf:
  SIL Open Font License 1.1, The Mozilla Foundation and Telefonica S.A.
  https://github.com/mozilla/Fira
