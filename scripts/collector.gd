extends Node2D


func _input(event):
	if not Global.DEBUG:
		return
	if event.is_action_pressed("ring"):
		ring()


func _ready():
	$AnimationPlayer.play("idle")


func bring_em_out():
	$BringOutSFX2.play()


func ring():
	$AnimationPlayer.play("ring")
	yield($AnimationPlayer, "animation_finished")
	$AnimationPlayer.play("idle")
