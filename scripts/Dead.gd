extends RigidBody2D

func _ready():
	add_to_group("falling_bodies")
	$Tween.interpolate_property(self, "modulate", Color(1,1,1), Color(1,0,0), $Timer.wait_time, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
	$Timer.start()

func _on_Timer_timeout():
	pass

func _on_Area2D_body_entered(body):
	if not is_in_group("falling_bodies"):
		return
	if body.is_in_group("floor_bodies") or body.is_in_group("floor"):
		remove_from_group("falling_bodies")
		add_to_group("floor_bodies")

#func _on_Area2D_body_entered(body):
#	if body is StaticBody2D:
#		print("Start rotting!")
#		$Tween.interpolate_property($Sprite, "modulate", Color(1,1,1), Color(1,0,0), $Timer.wait_time, Tween.TRANS_LINEAR, Tween.EASE_OUT)
#		$Tween.start()
#		$Timer.start()
