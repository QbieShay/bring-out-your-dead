extends Area2D

export var SPEED : int = 400
export var MAX_BODIES : int = 10

var body_count : int = 0
var left_button_pressed : bool = false
var right_button_pressed : bool = false


func _ready():
	print("Cart is in da place!")
	update_label()

	# Don't process until intro is done
	set_physics_process(false)


func _input(event):
	if not Global.DEBUG:
		return
	if event.is_action_pressed("ring"):
		start()


func _physics_process(delta):
	var direction = 0
	if Input.is_action_pressed("move_left") or left_button_pressed:
		direction -= 1
	if Input.is_action_pressed("move_right") or right_button_pressed:
		direction += 1

	position.x += SPEED * direction * delta
	$cart2/wheel.rotation += 15 * direction
	$cart2/wheel2.rotation += 15 * direction

	# Handle action key when overlapping areas (dump) or bodies (bodies :))
	# Dumps take precedence, so we check areas first.
	var overlap_areas : Array = get_overlapping_areas()
	var dump = []
	for a in overlap_areas:
		if a.is_in_group("dump"):
			dump.append(a)

	var overlap_bodies : Array = get_overlapping_bodies()
	if not dump.empty():
		$Hint.text = "Dump bodies"
	elif not overlap_bodies.empty():
		$Hint.text = "Collect body" if body_count < MAX_BODIES else "Full"
	else:
		$Hint.text = ""

	if Input.is_action_just_pressed("action") or (left_button_pressed and right_button_pressed):
		if not dump.empty():
			# Do something more clever eventually.
			dump_bodies(dump[0])
		elif not overlap_bodies.empty():
			# Do something more clever eventually.
			var obj = overlap_bodies[0]
			if obj.is_in_group("floor_bodies"):
				if body_count < MAX_BODIES:
					# Trigger pickup animation, takes time.
					# Remove floor body.
					add_body(obj)
				else:
					pass
					# Notify player that this body can't be picked up.


func _on_Cart_body_entered(body):
	if body.is_in_group("falling_bodies"):
		if body_count < MAX_BODIES:
			add_body(body)
			# Add body graphics to cart
		else:
			pass
			# Make animation showing that body bounces off cart


func start():
	set_physics_process(true)


func add_body(body):
	body_count += 1
	body.queue_free()
	update_label()
	_update_filling()


func dump_bodies(dump):
	# Should trigger dump animation, takes time.
	dump.add_bodies(body_count)
	body_count = 0
	update_label()
	_update_filling()


func update_label():
	$Label.text = "Bodies: %d" % body_count

func _update_filling():
	$cart2/filling.scale.y = float(body_count) / float(MAX_BODIES)


func _on_LeftScreenButton_pressed():
	left_button_pressed = true


func _on_LeftScreenButton_released():
	left_button_pressed = false


func _on_RightScreenButton_pressed():
	right_button_pressed = true


func _on_RightScreenButton_released():
	right_button_pressed = false
