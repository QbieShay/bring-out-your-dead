extends Node2D

const Dead = preload("res://scenes/Dead.tscn")

signal drop_the_dead(dead)

func _input(event):
	if not Global.DEBUG:
		return
	if event.is_action_pressed("spawn_bodies") and not event.is_echo():
		drop_the_dead()

func drop_the_dead():
	var dead = Dead.instance()
	dead.global_position = global_position
	var initial_speed = rand_range(30, 80)
	dead.linear_velocity = Vector2(0, 1).rotated(rand_range(0, 2*PI)) * initial_speed
	dead.angular_velocity = rand_range(-PI, PI)
	emit_signal("drop_the_dead", dead)
	$AnimationPlayer.play("open")
