extends Area2D

var body_count : int = 0

func add_bodies(new_bodies):
	body_count += new_bodies
	$Label.text = "Bodies: %d" % body_count

func _ready():
	print("Ready for dumping!")
