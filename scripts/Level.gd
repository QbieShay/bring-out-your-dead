extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()

func _on_Timer_timeout():
	var windows = get_tree().get_nodes_in_group("windows")
	windows[randi()%windows.size()].drop_the_dead()

func _physics_process(delta):
	if get_tree().get_nodes_in_group("floor_bodies").size() > 10:
		$Timer.stop()

